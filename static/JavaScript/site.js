

// for (let index = 0; index < 5; index++) {
//     console.log('index is: ', index);
// }

function GetData(){
    console.log('getting data');
    fetch('/api/data')
        .then(function(response) {
            console.log('inside function');
            return response.json();
        })
        .then(function(myJson) {
            $('#dataGrid').DataTable({
                 data : myJson,
                columns: [
                    { data: "Country" },
                    { data: "Region" },
                    { data: "Happiness Rank" },
                    { data: "Happiness Score" },
                    { data: "Standard Error" },
                    { data: "Economy (GDP per Capita)" },
                    { data: "Family" },
                    { data: "Health (Life Expectancy)" },
                    { data: "Freedom" },
                    { data: "Trust (Government Corruption)" },
                    { data: "Generosity" },
                    { data: "Dystopia Residual" }
                ]
            });
        });
        console.log('outside');
}

GetData();

$(document).ready( function () {

} );




