import random
import numpy as np
import pandas as pd
from bokeh.models import (HoverTool, FactorRange, Plot, LinearAxis, Grid,
                          Range1d)
from bokeh.models.glyphs import VBar
from bokeh.plotting import figure
#from bokeh.charts import Bar
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource
import flask
from datetime import datetime     
from flask import redirect,render_template,jsonify,request
from GenerateData import ReportRunner
import json
app = flask.Flask(__name__)
app.config["DEBUG"] = True
 
# creating connection Object which will contain SQL Server Connection  

data = ReportRunner.Report(ReportRunner)

@app.route('/')  
def home():  

    return render_template('index.html')

@app.route('/api/data')  
def getData():  
    return jsonify(data)

@app.route('/chart')
def chart():
    df = pd.DataFrame(data)
    x = df.get('Happiness Score')
    y = df.get('Family')
    plot = figure(plot_width=400, plot_height=400,title=None, toolbar_location="below")
    plot.line(x,y)

    script, div = components(plot)
    kwargs = {'script': script, 'div': div}
    kwargs['title'] = 'Happiness Score by Family'    
    return render_template('chart.html', **kwargs)

if __name__ == "__main__":
    app.run(debug=True)